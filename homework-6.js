
function createNewUser() {
  let firstName = prompt('Ваше имя?');
  let lastName = prompt('Ваша фамилия?');
  let birthday = prompt('Ваша дата рождения?');

  const newUser = {
    firstName,
    lastName,
    birthday,
    getLogin() {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    },
    getAge() {
      let now = new Date();
      let currYear = now.getFullYear();
      let userDate = +this.birthday.slice(0, 2);
      let userMonth = +this.birthday.slice(3, 5);
      let userYear = +this.birthday.slice(6);
      let birthDate = new Date(userYear, userMonth-1, userDate);
      let birthYear = birthDate.getFullYear();
      let age = currYear - birthYear;
      if (now < new Date(birthDate.setFullYear(currYear))) {
        age = age-1;
      }
      return age;
    },
    getPassword() {
      return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6));
    }
  }
  return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
console.log(user.getPassword());
console.log(user.getAge());
